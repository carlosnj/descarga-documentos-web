import urllib.request


class Robot:
    def __init__(self, _url):
        self.url = _url
        self.url_data = ""
        self.downloaded = False

    def retrieve(self):
        if not self.downloaded:
            with urllib.request.urlopen(self.url) as f:
                self.url_data = f.read(155)  # displays the first 155 bytes of the webpage.
                self.downloaded = True
            print(f"Downloading from {self.url}")
        else:
            print(self.url_data)

    def show(self):
        self.retrieve()

    def content(self):
        print(self.url_data.decode('utf-8'))  # we decode the bytes to get the string.


class Cache:
    def __init__(self):
        self.url_dict = {}

    def retrieve(self, _url):
        if _url not in self.url_dict.keys():
            r = Robot(_url)  # we use Robot() class to download the url content.
            r.retrieve()
            self.url_dict[_url] = r.url_data

        return self.url_dict[_url]

    def show(self, _url):
        print(self.retrieve(_url))

    def show_all(self):
        n = 1
        for i in self.url_dict.keys():
            print(str(n) + ".- " + i)
            n += 1

    def content(self, _url):
        print(self.retrieve(_url).decode('utf-8'))


try:
    print("---------- ROBOT CLASS ---------")
    r1 = Robot("https://raw.githubusercontent.com/dominictarr/random-name/master/first-names.txt")
    r2 = Robot("https://urjc.es")
    r1.retrieve()
    r1.show()
    r1.content()
    r2.retrieve()
    r2.show()
    r2.content()
    print('--------------------------------')

    print('---------- CACHE CLASS ---------')
    c1 = Cache()
    c2 = Cache()
    c1.retrieve("https://raw.githubusercontent.com/dominictarr/random-name/master/first-names.txt")
    c1.retrieve("https://gsyc.urjc.es//")
    c1.show("https://gsyc.urjc.es//")
    c1.show_all()
    c2.content("https://raw.githubusercontent.com/dominictarr/random-name/master/first-names.txt")
    c2.content("https://urjc.es")
    c2.show_all()
    print('--------------------------------')
except Exception as err:  # The except will raise probably because there is one or more invalid urls.
    print("Error:", err)

